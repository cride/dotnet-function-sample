USE [demo]
GO

/****** Object:  Table [dbo].[GeoZones]    Script Date: 30/10/2020 9:15:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GeoZones](
	[Id] [uniqueidentifier] NOT NULL,
	[CustomerCode] [varchar](100) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[ZoneGeometryJson] [varchar](2000) NOT NULL,
	[Description] [varchar](255) NULL,
	[created] [datetime] NOT NULL,
	[modified] [datetime] NOT NULL,
	[StartTime] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[EndTime] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
 CONSTRAINT [PK_GeoZones] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME ([StartTime], [EndTime])
) ON [PRIMARY]
GO



using System;
using Microsoft.Extensions.Logging;

namespace Forge.Function.Zones
{
    internal class LogMock : ILogger
    {
        public LogMock()
        {
        }

        IDisposable ILogger.BeginScope<TState>(TState state)
        {
            Console.Write("BeginScope:" + state.ToString());
            return null;
        }

        bool ILogger.IsEnabled(LogLevel logLevel)
        {
            Console.Write("IsEnabled:" + logLevel.ToString());
            return true;

        }

        void ILogger.Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            Console.WriteLine(logLevel.ToString() + " : " + formatter(state, exception));
        }
    }
}
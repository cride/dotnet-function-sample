using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace Forge.Function.Zones.Test
{
    public class TestDeleteZone
    {
        private string customerCode = "My Customer Code";
        private string zoneId = "My Zone Id";
        private string zoneName = "Zone Name";

        [Fact]
        public async void Test_DeleteZoneSQL_ValidRequest()
        {

            var sqlMock = Substitute.For<IZonesDbClient>();

            IActionResult result = await RunTestDeleteZone(sqlMock);
            Assert.Equal(typeof(OkResult), result.GetType());
            OkResult okObjectResult = (OkResult)result;
            await sqlMock.Received(1).DeleteZone(Arg.Is<string>(customerCode), Arg.Is<string>(zoneId));
        }

        private async Task<IActionResult> RunTestDeleteZone(IZonesDbClient sqlMock)
        {
            var noErrors = new List<Zone>();
            sqlMock.DeleteZone(Arg.Any<string>(), Arg.Any<string>()).Returns(
                 Task.FromResult(new Zone { Name = zoneName, Geometry = new Geometry { } })
            );
            var zoneApi = new ZonesAPI(sqlMock);

            HttpRequest request = Substitute.For<HttpRequest>();


            ILogger log = new LogMock();
            IActionResult result = await zoneApi.DeleteZone(request, log, customerCode, zoneId);
            return result;
        }
    }
}
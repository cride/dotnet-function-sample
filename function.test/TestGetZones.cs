using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace Forge.Function.Zones.Test
{
    public class TestGetZones
    {
        private string customerCode = "My Customer Code";
        private string zoneId = "My Zone Id";
        private string zoneName = "Zone Name";

        [Fact]
        public async void Test_GetZonesSQL_ValidRequest()
        {

            var sqlMock = Substitute.For<IZonesDbClient>();

            IActionResult result = await RunTestGetZone(sqlMock);
            Assert.Equal(typeof(OkObjectResult), result.GetType());
            OkObjectResult okObjectResult = (OkObjectResult)result;
            var zones = (List<Zone>)okObjectResult.Value;
            Assert.StartsWith(zoneName, zones[0].Name);
            await sqlMock.Received(1).GetZones(Arg.Is<string>(customerCode));
        }

        private async Task<IActionResult> RunTestGetZone(IZonesDbClient sqlMock)
        {
            var noErrors = new List<Zone>();
            sqlMock.GetZones(Arg.Any<string>()).Returns(
                 Task.FromResult(new List<Zone> { new Zone { Name = zoneName, Geometry = new Geometry { } } })
            );
            var zoneApi = new ZonesAPI(sqlMock);

            HttpRequest request = Substitute.For<HttpRequest>();


            ILogger log = new LogMock();
            IActionResult result = await zoneApi.GetZones(request, log, customerCode);
            return result;
        }
    }
}
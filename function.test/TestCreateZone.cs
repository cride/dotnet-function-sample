using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace Forge.Function.Zones.Test
{
    public class TestCreateZone
    {
        private string customerCode = "My Customer Code";

        [Fact]
        public async void Test_CreateZoneSQL_ValidRequest()
        {
            string body = @"
                {
                    ""zoneId"": ""aed35657-9423-48e4-b064-8976a764398c"",
                    ""geometry"": {
                    ""type"": ""Polygon"",
                    ""coordinates"": [
                        [
                        [
                            100.0,
                            0.0
                        ],
                        [
                            101.0,
                            0.0
                        ],
                        [
                            101.0,
                            1.0
                        ],
                        [
                            100.0,
                            1.0
                        ],
                        [
                            100.0,
                            0.0
                        ]
                        ]
                    ]
                    },
                    ""name"": ""My Zone Name"",
                    ""modified"": ""2019-10-11T09:24:32.82Z"",
                    ""created"": ""2019-10-11T09:24:32.82Z""
                }";

            var sqlMock = Substitute.For<IZonesDbClient>();

            IActionResult result = await RunTestCreateZone(body, sqlMock);
            Assert.Equal(typeof(OkResult), result.GetType());
            await sqlMock.Received(1).CreateZone(
                Arg.Is<string>(customerCode),
                Arg.Is<Zone>(x => x.Name == "My Zone Name"));
        }

        [Fact]
        public async void Test_CreateZoneSQL_InvalidRequest_WrongJson()
        {
            string body = @"
                {
                    ""message"": ""I am valid json""
                 }";

            var sqlMock = Substitute.For<IZonesDbClient>();

            IActionResult result = await RunTestCreateZone(body, sqlMock);
            Assert.Equal(result.GetType(), typeof(BadRequestObjectResult));
            BadRequestObjectResult badRequestObjectResult = (BadRequestObjectResult)result;
            Assert.Equal("ZoneError: Zone name cannot be empty", badRequestObjectResult.Value);
            await sqlMock.DidNotReceive().CreateZone(Arg.Any<string>(), Arg.Any<Zone>());
        }

        [Fact]
        public async void Test_CreateZoneSQL_InvalidRequest_MissingCoordinates()
        {
            string body = @"
               {
                    ""zoneId"": ""aed35657-9423-48e4-b064-8976a764398c"",
                    ""geometry"": {
                        ""type"": ""Polygon""                   
                    },
                    ""name"": ""1111 Chris new Land"",
                    ""modified"": ""2019-10-11T09:24:32.82Z"",
                    ""created"": ""2019-10-11T09:24:32.82Z""
                }";

            var sqlMock = Substitute.For<IZonesDbClient>();

            IActionResult result = await RunTestCreateZone(body, sqlMock);
            Assert.Equal(result.GetType(), typeof(BadRequestObjectResult));
            BadRequestObjectResult badRequestObjectResult = (BadRequestObjectResult)result;
            Assert.Equal("ZoneError: Zone coordinates cannot be empty", badRequestObjectResult.Value);

            await sqlMock.DidNotReceive().CreateZone(Arg.Any<string>(), Arg.Any<Zone>());
        }
        [Fact]
        public async void Test_CreateZoneSQL_InvalidRequest_InvalidCoordinates()
        {
            string body = @"
               {
                    ""zoneId"": ""aed35657-9423-48e4-b064-8976a764398c"",
                    ""geometry"": {
                    ""type"": ""Polygon"",
                    ""coordinates"": [
                        [
                        [
                            -100.0,
                            0.0
                        ],
                        [
                            101.0,
                            0.0
                        ],
                        [
                            101.0,
                            1.0
                        ],
                        [
                            100.0,
                            1.0
                        ],
                        [
                            100.0,
                            0.0
                        ]
                        ]
                    ]
                    },
                    ""name"": ""1111 Chris new Land"",
                    ""modified"": ""2019-10-11T09:24:32.82Z"",
                    ""created"": ""2019-10-11T09:24:32.82Z""
                }";

            var sqlMock = Substitute.For<IZonesDbClient>();

            IActionResult result = await RunTestCreateZone(body, sqlMock);
            Assert.Equal(result.GetType(), typeof(BadRequestObjectResult));
            BadRequestObjectResult badRequestObjectResult = (BadRequestObjectResult)result;
            Assert.Equal("ZoneError: points must form a closed linestring", badRequestObjectResult.Value);

            await sqlMock.DidNotReceive().CreateZone(Arg.Any<string>(), Arg.Any<Zone>());
        }
        [Fact]
        public async void Test_CreateZoneSQL_InvalidRequest_BadJson()
        {
            string body = @"
                {
                 I am an ""invalid  request and not json!23jp9ojds
                 ::"" ""
                 {{}";

            var sqlMock = Substitute.For<IZonesDbClient>();

            IActionResult result = await RunTestCreateZone(body, sqlMock);
            Assert.Equal(result.GetType(), typeof(BadRequestObjectResult));
            BadRequestObjectResult badRequestObjectResult = (BadRequestObjectResult)result;
            Assert.StartsWith("ZoneError: Invalid character after parsing property name", badRequestObjectResult.Value.ToString());
            await sqlMock.DidNotReceive().CreateZone(Arg.Any<string>(), Arg.Any<Zone>());
        }

        private async Task<IActionResult> RunTestCreateZone(string body, IZonesDbClient sqlMock)
        {
            var noErrors = new List<Zone>();
            sqlMock.CreateZone(Arg.Any<string>(), Arg.Any<Zone>()).Returns(
                 Task.FromResult(noErrors)
            );
            var zoneApi = new ZonesAPI(sqlMock);

            HttpRequest request = Substitute.For<HttpRequest>();

            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(body));
            request.Body.Returns(stream);
            ILogger log = new LogMock();
            IActionResult result = await zoneApi.CreateZone(request, customerCode, log);
            return result;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Forge.Function.Zones
{
    public class ZonesDbClient : IZonesDbClient
    {
        private SqlConnection getConnection()
        {
            //Establish connection string
            var connectionString = Environment.GetEnvironmentVariable("sqldb_connection");
            //Begin using the connection to access the database
            return new SqlConnection(connectionString);
        }

        public async Task CreateZone(string customerCode, Zone zone)
        {
            //Begin using the connection to access the database
            using (var connection = getConnection())
            {
                //SQL string used to update the database with values
                var sql = @"INSERT INTO dbo.GeoZones(Id, CustomerCode, ZoneGeometryJson, Name, created, modified) VALUES(NEWID(), @customerCode, @geometry, @name, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    //Assign values to items in sql string
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.AddWithValue("@customerCode", customerCode);
                    cmd.Parameters.AddWithValue("@geometry", JsonConvert.SerializeObject(zone.Geometry));
                    cmd.Parameters.AddWithValue("@name", zone.Name);

                    //Open connection and execute query
                    await connection.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();
                }
            }
        }

        public async Task<Zone> GetZone(string customerCode, string zoneId)
        {
            using (var connection = getConnection())
            {
                var sql = @"SELECT id, name, zoneGeometryJson, created, modified FROM dbo.GeoZones WHERE Id = @zoneId AND CustomerCode = @customerCode";
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    //Assign values to items in sql string
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.AddWithValue("@zoneId", zoneId);
                    cmd.Parameters.AddWithValue("@customerCode", customerCode);

                    await connection.OpenAsync();
                    // Execute the command
                    var reader = await cmd.ExecuteReaderAsync();

                    if (reader.HasRows)
                    {
                        reader.Read();
                        return SQLReaderToZone(reader);
                    }
                    throw new ArgumentException("Could not find Zone");
                }
            }
        }

        public async Task<List<Zone>> GetZones(string customerCode)
        {
            using (var connection = getConnection())
            {
                var sql = @"SELECT id, name, zoneGeometryJson, created, modified FROM dbo.GeoZones WHERE CustomerCode = @customerCode";

                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    //Assign values to items in sql string
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.AddWithValue("@customerCode", customerCode);

                    await connection.OpenAsync();
                    // Execute the command
                    var reader = await cmd.ExecuteReaderAsync();

                    var zones = new List<Zone>();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Zone z = SQLReaderToZone(reader);
                            zones.Add(z);
                        }
                    }
                    return zones;
                }
            }
        }

        private static Zone SQLReaderToZone(SqlDataReader reader)
        {
            Zone z = new Zone();
            z.ZoneId = ((Guid)reader["id"]).ToString();
            z.Name = (string)reader["name"];
            var geometryString = (string)reader["zoneGeometryJson"];
            z.Geometry = JsonConvert.DeserializeObject<Geometry>(geometryString);
            z.Created = (DateTime)reader["created"];
            z.Modified = (DateTime)reader["modified"];
            return z;
        }

        public async Task UpdateZone(string customerCode, string zoneId, Zone zone)
        {
            using (var connection = getConnection())
            {
                //SQL string used to update the database with values
                var sql = @"IF EXISTS(SELECT * FROM dbo.GeoZones WHERE Id = @zoneId AND CustomerCode = @customerCode)
                        UPDATE dbo.GeoZones
                        SET ZoneGeometryJson = @geometry, name = @name, modified = CURRENT_TIMESTAMP
                        WHERE Id = @zoneId AND CustomerCode = @customerCode";
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    //Assign values to items in sql string
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.AddWithValue("@geometry", JsonConvert.SerializeObject(zone.Geometry));
                    cmd.Parameters.AddWithValue("@zoneId", zoneId);
                    cmd.Parameters.AddWithValue("@customerCode", customerCode);
                    cmd.Parameters.AddWithValue("@name", zone.Name);

                    //Open connection and execute query
                    await connection.OpenAsync();
                    int rowsAffected = await cmd.ExecuteNonQueryAsync();

                    if (rowsAffected != 1)
                    {
                        throw new ArgumentException("Zone could not be updated");
                    }
                }
            }
        }

        public async Task DeleteZone(string customerCode, string zoneId)
        {

            //Begin using the connection to access the database
            using (var connection = getConnection())
            {
                //SQL string used to update the database with values
                var sql = @"IF EXISTS(SELECT * FROM dbo.GeoZones WHERE Id = @zoneId AND CustomerCode = @customerCode)
                        DELETE FROM dbo.GeoZones
                        WHERE Id = @zoneId AND CustomerCode = @customerCode";
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    //Assign values to items in sql string
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.AddWithValue("@zoneId", zoneId);
                    cmd.Parameters.AddWithValue("@customerCode", customerCode);

                    //Open connection and execute query
                    await connection.OpenAsync();
                    int numberDeleted = await cmd.ExecuteNonQueryAsync();

                    if (numberDeleted != 1)
                    {
                        throw new ArgumentException("Zone could not be updated");
                    }
                }
            }
        }
    }
}
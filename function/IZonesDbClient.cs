using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forge.Function.Zones
{
    public interface IZonesDbClient
    {
        Task CreateZone(string customerCode, Zone zone);
        Task DeleteZone(string customerCode, string zoneId);
        Task<Zone> GetZone(string customerCode, string zoneId);
        Task<List<Zone>> GetZones(string customerCode);
        Task UpdateZone(string customerCode, string zoneId, Zone zone);
    }
}
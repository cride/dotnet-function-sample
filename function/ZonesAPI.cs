using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Serialization;
using NetTopologySuite.IO;
using NetTopologySuite.Geometries;

namespace Forge.Function.Zones
{
    public class ZonesAPI
    {
        private IZonesDbClient _zoneDbClient;

        public ZonesAPI(IZonesDbClient zoneDbClient)
        {
            _zoneDbClient = zoneDbClient;
        }
        private static readonly GeoJsonReader _geoReader = new GeoJsonReader();

        [FunctionName("CreateZone")]
        public async Task<IActionResult> CreateZone(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "zones/{customerCode}")] HttpRequest req,
            string customerCode,
            ILogger log)
        {
            try
            {
                log.LogInformation($"*** Zones.CreateZone. Customer Code:{customerCode}");
                //Get Request Body
                string body = await new StreamReader(req.Body).ReadToEndAsync();

                //Deserialise the body to the correct format
                Zone zone = JsonConvert.DeserializeObject<Zone>(body);

                //Check the geoJsonString can be converted to a Polygon object
                IsValidZone(zone, log);

                //Conect to database and add new zone entry
                await _zoneDbClient.CreateZone(customerCode, zone);
                return new OkResult();

            }
            catch (ArgumentException e)
            {
                return HandleException(log, e);
            }
            catch (JsonReaderException e)
            {
                return HandleException(log, e);
            }
        }

        [FunctionName("UpdateZone")]
        public async Task<IActionResult> UpdateZone(
            [HttpTrigger(AuthorizationLevel.Function, "put", Route = "zones/{customerCode}/{zoneId}")] HttpRequest req,
            ILogger log, String customerCode, String zoneId)
        {
            try
            {
                //Get Request Body
                string body = await new StreamReader(req.Body).ReadToEndAsync();

                //Deserialise the body to the correct format
                var zone = JsonConvert.DeserializeObject<Zone>(body);

                //Check the geoJsonString can be converted to a Polygon object
                IsValidZone(zone, log);

                //Conect to database and update new zone entry
                await _zoneDbClient.UpdateZone(customerCode, zoneId, zone);
                return new OkResult();
            }
            catch (ArgumentException e)
            {
                return HandleException(log, e);
            }
            catch (JsonReaderException e)
            {
                return HandleException(log, e);
            }

        }

        private static BadRequestObjectResult HandleException(ILogger log, Exception e)
        {
            //Return error messages if an error occurs 
            log.LogError("ZoneError", e, e.Message);
            return new BadRequestObjectResult("ZoneError: " + e.Message);
        }

        [FunctionName("DeleteZone")]
        public async Task<IActionResult> DeleteZone(
            [HttpTrigger(AuthorizationLevel.Function, "delete", Route = "zones/{customerCode}/{zoneId}")] HttpRequest req,
            ILogger log, string customerCode, string zoneId)
        {
            try
            {
                await _zoneDbClient.DeleteZone(customerCode, zoneId);
                return new OkResult();
            }
            catch (ArgumentException)
            {
                return new BadRequestObjectResult("Delete failed");
            }
        }

        [FunctionName("GetZone")]
        public async Task<IActionResult> GetZone(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "zones/{customerCode}/{zoneId}")] HttpRequest req,
            ILogger log, string customerCode, string zoneId)
        {
            try
            {
                Zone zone = await _zoneDbClient.GetZone(customerCode, zoneId);
                return (ActionResult)new OkObjectResult(zone);
            }
            catch (ArgumentException)
            {
                return new BadRequestObjectResult("Zone not found");
            }
        }

        [FunctionName("GetZones")]
        public async Task<IActionResult> GetZones(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "zones/{customerCode}")] HttpRequest req,
            ILogger log, string customerCode)
        {
            List<Zone> zones = await _zoneDbClient.GetZones(customerCode);
            return (ActionResult)new OkObjectResult(zones);
        }

        /// <summary>
        /// Returns true of the zone is complete and polygonJsonString is a valide GoeJson polygon otherwise throws and Argument Exception
        /// </summary>
        public bool IsValidZone(Zone zone, ILogger log)
        {
            //Check the basic attributes
            if (zone.Name == null || zone.Name.Trim() == "")
            {
                throw new ArgumentException("Zone name cannot be empty");
            }
            if (zone.Geometry == null)
            {
                throw new ArgumentException("Zone geometry cannot be empty");
            }

             if (zone.Geometry.coordinates == null)
            {
                throw new ArgumentException("Zone coordinates cannot be empty");
            }

            string polygonJsonString = JsonConvert.SerializeObject(zone.Geometry);

            try
            {
                Polygon polygon = _geoReader.Read<NetTopologySuite.Geometries.Polygon>(polygonJsonString);
                //Only add the zone if the above works
                return true;
            }
            catch (ArgumentException e)
            {
                log.LogError("Polygon Json String: " + polygonJsonString + " could not be converted to a Polygon: " + e.ToString());
                throw e;
            }
        }
    }
}

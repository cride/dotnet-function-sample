﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;

[assembly: FunctionsStartup(typeof(Forge.Function.Zones.Startup))]

namespace Forge.Function.Zones
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            // Register the Singleton Services
            builder.Services.AddSingleton((s) =>
            {
                return (IZonesDbClient) new ZonesDbClient();
            });

          
        }
    }
}
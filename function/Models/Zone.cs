using System;

namespace Forge.Function.Zones
{
    public class Zone
    {
        public string ZoneId { get; set; }
        public Geometry Geometry { get; set; }
        public string Name { get; set; }
        public DateTime Modified { get; internal set; }
        public DateTime Created { get; internal set; }
    }

}
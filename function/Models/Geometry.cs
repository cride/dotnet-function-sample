using System.Collections.Generic;

namespace Forge.Function.Zones
{
    public class Geometry
    {
        public string type { get; set; }
        public List<List<List<double>>> coordinates { get; set; }

    }
}